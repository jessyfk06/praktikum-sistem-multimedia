# No 1
Produk ini adalah sebuah perangkat lunak yang dirancang untuk menghapus green screen (atau warna lain yang dijadikan kunci) dari gambar atau video, dan kemudian menggantikan latar belakang dengan gambar atau video yang diinginkan. Produk ini memberikan solusi yang efektif dan efisien dalam pengeditan video dengan menggunakan teknik chroma keying.

Dengan menggunakan produk ini, pengguna dapat dengan mudah menghilangkan latar belakang hijau yang sering digunakan dalam proses chroma keying. Produk ini menyediakan fitur yang memungkinkan pengguna untuk mengatur ambang batas warna hijau yang akan dihapus, sehingga dapat disesuaikan dengan kondisi pengambilan gambar yang berbeda.

Selain penghapusan green screen, produk ini juga memberikan opsi untuk menambahkan latar belakang yang diinginkan. Pengguna dapat memasukkan gambar atau video latar belakang yang sesuai dengan preferensi mereka, baik itu berupa gambar statis, video looping, atau video dengan efek khusus.

Produk ini dirancang dengan antarmuka yang user-friendly, yang memungkinkan pengguna untuk mengakses dan mengoperasikan program dengan mudah. Melalui Command Line Interface (CLI), pengguna dapat menjalankan program dan mengatur parameter sesuai kebutuhan mereka. Produk ini juga mendukung berbagai format file video dan gambar yang umum digunakan.

Keunggulan produk ini terletak pada kemampuan untuk menghilangkan green screen secara otomatis dan menggantikannya dengan latar belakang yang diinginkan, memberikan fleksibilitas dan kreativitas dalam pengeditan video. Dengan penggunaan yang tepat, produk ini dapat menghasilkan video yang menarik, profesional, dan sesuai dengan kebutuhan pengguna, baik itu dalam produksi video, konten kreatif, pemasaran, atau keperluan lainnya.

Produk ini memungkinkan pengguna untuk menghemat waktu dan upaya dalam proses pengeditan video, karena pengguna tidak perlu menggunakan perangkat keras khusus atau software yang rumit. Dengan menggunakan produk ini, pengguna dapat dengan mudah mencapai hasil yang diinginkan dalam mengedit video dengan penghapusan green screen dan penambahan latar belakang yang sesuai.

##### List Use Case :
1. Produksi Video dan Film: Produk ini dapat digunakan dalam produksi video dan film untuk menghapus green screen pada adegan yang direkam menggunakan teknik chroma keying. Pengguna dapat dengan mudah mengganti latar belakang dengan menggunakan gambar atau video yang sesuai dengan tema atau suasana yang diinginkan.
2. Pembuatan Konten Kreatif: Para kreator konten seperti YouTuber, vlogger, atau animator dapat menggunakan produk ini untuk menghilangkan green screen dalam video mereka. Mereka dapat memberikan latar belakang yang menarik atau membuat efek khusus yang unik untuk meningkatkan kualitas dan kreativitas konten mereka.
3. Presentasi dan Video Konferensi: Dalam situasi presentasi atau video konferensi, produk ini dapat membantu dalam memberikan tampilan yang lebih profesional dan menarik. Pengguna dapat menghapus green screen di sekitar diri mereka dan menambahkan latar belakang yang relevan dengan topik presentasi atau suasana yang diinginkan.
4. Pemasaran dan Iklan: Dalam kegiatan pemasaran dan iklan, produk ini dapat digunakan untuk menciptakan video yang menarik dengan latar belakang yang sesuai dengan merek atau pesan yang ingin disampaikan. Dengan menghapus green screen dan menambahkan latar belakang yang tepat, video iklan dapat menjadi lebih menonjol dan memikat perhatian audiens.
5. Produksi Konten Digital: Bagi para desainer grafis, animator, atau seniman digital, produk ini dapat digunakan untuk menciptakan konten digital seperti animasi, ilustrasi, atau gambar dengan latar belakang yang lebih menarik. Mereka dapat menggabungkan elemen-elemen grafis dengan gambar atau video latar belakang yang relevan untuk menciptakan karya yang menarik dan kreatif.

Dengan menghapus green screen dan menambahkan latar belakang yang sesuai, pengguna dapat membuat video yang menghibur dan menghasilkan konten yang menarik untuk dibagikan dengan orang lain.

# No 2

Berikut merupakan program sederhana (CLI) remove green screen & add background for video. Implementasi sederhana dari green screen effect menggunakan library OpenCV dalam Python. 
Flowchart :

```mermaid
flowchart TD
    A[Memulai] --> B[Membuka Video]
    B --> C[Memuat Gambar Latar Belakang]
    C --> D[Mendefinisikan Fungsi Kompresi Video]
    D --> E[Membuat Trackbars]
    E --> F[Mulai Loop]
    F --> G[Membaca Frame]
    G --> H[Cek Ret]
    H -- Ya --> I[Memodifikasi Ukuran Frame]
    I --> J[Memodifikasi Ukuran Gambar Latar Belakang]
    J --> K[Konversi ke HSV]
    K --> L[Mendapatkan Nilai Trackbars]
    L --> M[Mendefinisikan Rentang Hijau]
    M --> N[Segmentasi Warna]
    N --> O[Operasi Bitwise]
    O --> P[Tampilan Frame Asli]
    O --> Q[Tampilan Masker]
    O --> R[Tulis Frame ke Video Output]
    R --> S[Tampilan Green Screen]
    S --> T[Cek Tombol q]
    T -- Ya --> U[Akhir Loop]
    U --> V[Tutup Video]
    V --> W[Tutup Video Output]
    W --> X[Akhir Program]
    T -- Tidak --> F

```

Flowchart di atas menggambarkan alur kerja dari kode yang diberikan. Langkah-langkahnya adalah sebagai berikut:

1. Memulai program.
2. Membuka video yang akan diproses.
3. Memuat gambar latar belakang yang akan digunakan untuk green screen.
4. Mendefinisikan fungsi kompresi video.
5. Membuat trackbars untuk mengatur nilai batas hijau.
6. Memulai loop utama.
7. Membaca frame dari video.
8. Memeriksa keberhasilan membaca frame.
9. Jika frame berhasil dibaca, langkah-langkah berikutnya akan dilakukan. Jika tidak, program akan keluar dari loop.
10. Memodifikasi ukuran frame menjadi 640x480 piksel.
11. Memodifikasi ukuran gambar latar belakang sesuai dengan ukuran frame.
12. Mengonversi frame ke ruang warna HSV.
13. Mendapatkan nilai dari trackbars.
14. Mendefinisikan rentang hijau berdasarkan nilai trackbars.
15. Melakukan segmentasi warna menggunakan rentang hijau untuk menghasilkan masker.
16. Melakukan operasi bitwise antara frame dan masker.
17. Menampilkan frame asli.
18. Menampilkan masker.
19. Menulis frame hasil green screen ke video output.
20. Menampilkan tampilan green screen.
21. Memeriksa apakah tombol "q" ditekan. Jika ya, program keluar dari loop dan berakhir.
22. Kembali ke langkah 7 untuk membaca frame berikutnya.
23. Setelah loop selesai, program menutup video dan video output.
24. Program berakhir.

# No 3

##### Link Source Code : https://github.com/JessyFaujiyyahK/UAS-Video

![](program_video.gif)

# No 4

##### Link Youtube : https://youtu.be/poXx8FRnGA0

# No 5
Video processing adalah proses manipulasi dan analisis data visual yang ada dalam sebuah video. Proses ini mencakup berbagai teknik seperti pemrosesan citra, pengolahan sinyal, dan kompresi data untuk menghasilkan output video yang diinginkan. Dalam konteks produk ini, konsep video processing digunakan untuk menghilangkan green screen dan menambahkan latar belakang pada video dengan menggunakan teknik chroma keying.

Video compression adalah proses mengurangi ukuran file video dengan menghilangkan atau mengurangi redundansi informasi yang tidak diperlukan. Tujuan utama dari video compression adalah untuk mengurangi ukuran file video tanpa mengorbankan kualitas visual yang signifikan. Dalam konteks produk ini, konsep video compression digunakan untuk mengompresi video output agar ukuran filenya lebih efisien dan mudah diakses.

Video processing tool development merujuk pada pengembangan perangkat lunak atau aplikasi yang digunakan untuk melakukan pemrosesan video dengan berbagai fungsi dan fitur yang sesuai dengan kebutuhan pengguna. Dalam konteks produk ini, konsep video processing tool development melibatkan pembuatan perangkat lunak yang dapat menghapus green screen dan menambahkan latar belakang pada video secara otomatis. Hal ini melibatkan implementasi algoritma, pengolahan citra, kompresi video, serta pengembangan antarmuka pengguna yang intuitif dan mudah digunakan.

Dalam produk ini, video processing tool development menggabungkan konsep-konsep tersebut untuk menciptakan perangkat lunak yang dapat menghapus green screen dari video dengan menggunakan teknik chroma keying. Selain itu, produk ini juga melakukan kompresi video untuk menghasilkan file video output yang lebih efisien. Pengembangan perangkat lunak ini melibatkan pemrograman, optimisasi algoritma, dan pengujian untuk memastikan kinerja yang baik, kualitas visual yang optimal, serta kemudahan penggunaan bagi pengguna akhir.

Melalui konsep video processing, video compression, dan video processing tool development, produk ini menyediakan solusi yang praktis dan efisien untuk mengedit video dengan penghapusan green screen dan penambahan latar belakang yang sesuai dengan preferensi pengguna.

Kecerdasan buatan (Artificial Intelligence/AI) tidak secara langsung terlibat dalam kodingan tersebut, namun dapat terkait dengan penggunaan kodingan tersebut dalam konteks pengembangan sistem cerdas atau aplikasi yang menggunakan teknik pengolahan gambar dan video.

Dalam hal ini, kodingan tersebut menggunakan library OpenCV yang merupakan salah satu pustaka populer dalam bidang pengolahan gambar dan video. OpenCV menyediakan berbagai fungsi dan algoritma untuk manipulasi gambar dan video, termasuk deteksi objek, segmentasi, pengenalan wajah, dan banyak lagi. Dengan menggunakan algoritma-algoritma tersebut, kita dapat mengimplementasikan sistem cerdas yang mampu memproses data visual.

Dalam kodingan tersebut, misalnya, teknik chroma keying digunakan untuk menghilangkan latar belakang hijau pada video. Ini adalah salah satu contoh penggunaan kecerdasan buatan di mana sistem dapat memahami dan memanipulasi konten visual untuk menghasilkan efek yang diinginkan. Dalam hal ini, sistem secara otomatis mendeteksi dan menghapus latar belakang hijau menggunakan nilai batas rendah dan batas atas yang dapat disesuaikan oleh pengguna melalui trackbar.

Dengan menggabungkan kodingan ini dengan teknik-teknik AI lainnya, seperti pengenalan objek atau deteksi wajah, dapat mengembangkan sistem cerdas yang lebih kompleks, seperti aplikasi pengeditan video interaktif atau sistem augmented reality.

Dalam keseluruhan, kodingan tersebut mencerminkan penerapan AI dalam pengolahan gambar dan video untuk mencapai hasil yang cerdas dan terautomasi.

# No 6
Emotion Detection Web Realtime adalah sebuah sistem yang dirancang untuk mendeteksi emosi secara langsung (real-time) melalui kamera web. Sistem ini menggunakan teknik deep learning dan pengolahan citra untuk mengenali ekspresi wajah dan mengklasifikasikan emosi yang terkait.

Sistem Emotion Detection Web Realtime memiliki beberapa fitur utama. Pertama, sistem ini menggunakan kamera web sebagai sumber input untuk mendapatkan gambar wajah pengguna secara langsung. Pengguna dapat dengan mudah mengakses sistem melalui browser web mereka tanpa memerlukan instalasi atau konfigurasi tambahan.

Kedua, sistem ini menerapkan teknik deep learning yang telah dilatih sebelumnya untuk mengenali emosi dari ekspresi wajah. Proses ini melibatkan analisis dan ekstraksi fitur dari gambar wajah, yang kemudian digunakan untuk mengklasifikasikan emosi yang ada, seperti 'Anger', 'Neutral', 'Fear', 'Happy', 'Sad', dan 'Surprise'.

Selain itu, sistem Emotion Detection Web Realtime menggunakan algoritma dan model yang telah dilatih sebelumnya untuk mencapai tingkat akurasi yang baik dalam deteksi emosi. Melalui proses pelatihan menggunakan dataset yang representatif, model mampu mengenali pola dan ciri-ciri yang berkaitan dengan emosi tertentu, sehingga memberikan hasil deteksi yang akurat.

Ketiga, sistem ini menawarkan respons yang cepat dan waktu pemrosesan yang minimal. Dengan menerapkan teknik real-time, sistem dapat secara instan menganalisis gambar wajah yang diperoleh dari kamera web, mengidentifikasi emosi yang ada, dan memberikan hasil deteksi dalam waktu nyaris seketika.

Emotion Detection Web Realtime dapat digunakan dalam berbagai konteks. Misalnya, sistem ini dapat diterapkan dalam pengawasan kesehatan mental, analisis sentimen, penelitian psikologi, atau pengalaman pengguna interaktif. Dalam konteks bisnis, sistem ini dapat memberikan wawasan berharga untuk penilaian respons pelanggan, evaluasi produk, atau pengembangan konten yang disesuaikan dengan emosi pengguna. Dengan kemampuan untuk mendeteksi emosi secara real-time melalui kamera web, Emotion Detection Web Realtime memberikan nilai tambah dalam menghadirkan pengalaman interaktif yang lebih mendalam dan personal bagi pengguna.

##### List Use Case :
1. Pengawasan Kesehatan Mental: Sistem ini dapat digunakan dalam aplikasi kesehatan mental untuk memantau dan mengidentifikasi perubahan emosi pada pasien. Hal ini dapat membantu dalam mendeteksi gejala depresi, kecemasan, atau masalah kesehatan mental lainnya secara real-time.
2. Analisis Sentimen Media Sosial: Dalam analisis sentimen, sistem ini dapat digunakan untuk menganalisis ekspresi wajah pengguna di media sosial, seperti foto profil atau video pendek, untuk mengetahui tanggapan emosional mereka terhadap suatu topik atau produk tertentu.
3. Penelitian Psikologi dan Emosi: Emotion Detection Web Realtime dapat digunakan dalam penelitian psikologi untuk mengumpulkan data emosi secara real-time. Ini membantu dalam mempelajari respons emosional dalam situasi atau percobaan tertentu dan memberikan pemahaman lebih dalam tentang perilaku manusia.
4. Pengalaman Pengguna Interaktif: Sistem ini dapat digunakan dalam pengembangan aplikasi atau situs web dengan antarmuka interaktif yang merespons emosi pengguna. Contohnya, aplikasi permainan yang mengadaptasi tantangan dan tingkat kesulitan berdasarkan emosi pengguna yang terdeteksi.
5. Evaluasi Respons Pelanggan: Emotion Detection Web Realtime dapat digunakan dalam konteks bisnis untuk mendapatkan wawasan tentang respons pelanggan terhadap produk, layanan, atau kampanye pemasaran. Dengan melacak ekspresi wajah pelanggan, perusahaan dapat memahami lebih baik bagaimana pelanggan merasakan dan merespons produk mereka.

# No 7 

##### Link Source Code : https://github.com/JessyFaujiyyahK/UAS-PrakSisMul

# No 8 

##### Link Youtube : https://youtu.be/a0K7QEeJfFQ

# No 9
Proses Research and Development (R&D) dalam pengembangan model deteksi emosi menggunakan deep learning dan implementasi pada web server melibatkan serangkaian tahapan yang terstruktur. Berikut adalah penjelasan mengenai proses R&D yang dilakukan:

#### 1. Penelitian Awal
Tahap ini melibatkan penelitian dan studi literatur tentang metode dan teknik yang relevan dalam deteksi emosi dan penggunaan deep learning. Tujuannya adalah memahami konsep dasar, pendekatan yang telah digunakan sebelumnya, dan teknologi terbaru dalam domain ini. Penelitian awal ini menjadi dasar untuk merancang pendekatan yang tepat dalam pengembangan model deteksi emosi.

#### 2. Pengumpulan Data
Tahap berikutnya adalah pengumpulan dataset gambar wajah yang mencakup berbagai ekspresi emosi. Dataset ini harus mewakili variasi emosi yang ada dalam populasi secara umum.

#### 3. Preprocessing Data
Setelah pengumpulan data, tahap ini melibatkan pra-pemrosesan data untuk mempersiapkannya menjadi format yang dapat digunakan oleh model. Proses preprocessing data mencakup langkah-langkah seperti penyesuaian ukuran gambar, normalisasi, peningkatan kontras, atau penerapan teknik augmentasi data untuk meningkatkan keberagaman dataset.

#### 4. Pembangunan Model
Tahap ini melibatkan pembangunan dan pelatihan model deep learning untuk deteksi emosi. Model ini dapat berbasis arsitektur jaringan saraf tiruan seperti Convolutional Neural Network (CNN) . Langkah-langkah dalam tahap ini meliputi pemilihan arsitektur model yang sesuai, inisialisasi parameter, pelatihan model menggunakan algoritma seperti backpropagation, dan penyesuaian parameter untuk mencapai akurasi yang diinginkan.

##### Flowchart Pembangunan Model :
```mermaid
    flowchart TD
        A[Import library numpy, pandas, dan cv2] --> B[Baca file csv dan tampilkan data pertama]
        B --Periksa panjang data kolom 'pixels'--> C
        C[Import label_map nama-nama emosi] --> D[Tampilkan gambar pertama]
        B --> E[Buat fungsi getData]
        E --> F[Panggil getData]
        F --Ubah nilai X--> G[Normalisasi nilai X]
        G --> H[Ubah nilai label menjadi one-hot encoding]
        H --> I[Tampilkan 5 gambar acak]
        G --> J[Ubah bentuk variabel X]
        J --Import library to_categorical--> K
        H --> K
        K --Buat model neural network--> L[Layer CNN, Layer CNN, Layer CNN, Flatten, Dense]
        L --> M[Compile model]
        M --> N[Lakukan training model]
        N --> O[Cek akurasi model]
        O --> P[Simpan model]

```
##### Penjelasan Cara Kerja :
1. Import library numpy, pandas, dan cv2.
2. Menggunakan pandas untuk membaca file csv yang berisi data tentang ekspresi wajah dan menyimpannya ke dalam variabel df. Kemudian menampilkan 5 data pertama dari df menggunakan method head().
3. Melakukan pengecekan panjang data pada kolom 'pixels' pada baris pertama df dengan menggunakan method len(), dan menampilkan hasilnya. Hasilnya adalah 2304, yang merupakan hasil dari 48 x 48.
4. Membuat label_map yang berisi nama-nama emosi, yaitu 'Anger', 'Neutral', 'Fear', 'Happy', 'Sad', dan 'Surprise'.
5. Import library matplotlib.pyplot, dan menampilkan gambar dari baris pertama df dengan menggunakan method imshow(), dan menambahkan label yang sesuai dengan label_map.
6. Membuat fungsi getData(path) yang menerima parameter path berisi path menuju file csv yang berisi data tentang ekspresi wajah. Kemudian membaca file csv tersebut menggunakan pandas dan menyimpannya ke dalam variabel df. Variabel X dan y diinisialisasi dengan list kosong. Variabel-variabel tersebut akan diisi dengan nilai-nilai pixel dan label. Kemudian dilakukan iterasi pada df, dimana setiap iterasi akan mengecek nilai pada kolom 'emotion' pada baris yang sedang diperiksa. Jika nilainya tidak sama dengan 1, maka akan dilakukan pengecekan apakah nilai tersebut sudah mencapai batas maksimum 4000 untuk setiap jenis emosi. Jika belum mencapai, maka nilai pada kolom 'emotion' akan disimpan ke dalam variabel y, dan nilai pada kolom 'pixels' akan diubah menjadi list of integer dan disimpan ke dalam variabel X. Selain itu, nilai untuk jenis emosi tersebut akan diincrement untuk menghitung batas maksimum yang sudah dicapai. Jika nilai pada kolom 'emotion' sama dengan 1, maka tidak akan dilakukan apa-apa.
7. Memanggil fungsi getData() dengan parameter path yang berisi path menuju file csv yang berisi data tentang ekspresi wajah. Hasil yang didapatkan adalah nilai-nilai piksel yang sudah dinormalisasi dan labelnya yang sudah diubah, dan disimpan ke dalam variabel X dan y.
8. Mengubah nilai pada variabel X menjadi float dan membaginya dengan 255.0 untuk melakukan normalisasi.
9. Membuat variabel y_o yang akan diisi dengan nilai label yang sudah diubah dengan mengubah nilai 6 menjadi 1, dan nilai-nilai lainnya tetap sama. Kemudian menampilkan jumlah setiap label menggunakan method unique().
10. Menampilkan 5 gambar secara acak dari variabel X beserta labelnya menggunakan library matplotlib.
11. Mengubah bentuk variabel X menjadi array 4 dimensi dengan ukuran (no_of_images, height, width, coloar_map).
12. Mengimport library to_categorical dari keras.utils.
13. Mengubah nilai pada variabel y_o menjadi bentuk one-hot encoding dengan menggunakan method to_categorical() dari library keras.utils.
14. Menggunakan Keras untuk membuat model neural network untuk klasifikasi gambar. 
15. Mengambil gambar grayscale dengan ukuran 48 x 48 piksel sebagai input dan mengeluarkan label kelas gambar yang terdiri dari 6 kelas yang berbeda.
16. Import library yang diperlukan: Code diawali dengan mengimport library yang dibutuhkan yaitu Keras dan optimizer untuk mengoptimalkan model.
17. Membuat objek model: Objek model dibuat dengan menggunakan Sequential() yang merupakan struktur model paling sederhana dan sequential, dimana layer-layer neural network ditambahkan secara berurutan.
18. Menambahkan layer Convolutional Neural Network (CNN): Pertama, ditambahkan layer Conv2D dengan 64 kernel, filter berukuran 5x5, fungsi aktivasi 'relu', padding 'same', dan input shape sebesar 48x48x1. Kemudian, ditambahkan lagi Conv2D layer dengan spesifikasi yang sama. Selanjutnya, ditambahkan BatchNormalization dan Activation dengan fungsi aktivasi 'relu'. Terakhir, ditambahkan MaxPooling2D layer dengan pool size 2x2.
19. Menambahkan layer CNN: Ditambahkan lagi layer CNN dengan spesifikasi sama seperti sebelumnya, namun dengan 128 kernel.
20. Menambahkan layer CNN: Ditambahkan lagi layer CNN dengan filter berukuran 3x3, 256 kernel, fungsi aktivasi 'relu', padding 'same', dan dilakukan MaxPooling2D.
21. Melakukan Flatten(): Dilakukan flatten pada output layer sebelumnya agar dapat dihubungkan ke Dense layer.
22. Menambahkan Dense layer: Ditambahkan Dense layer dengan 6 unit neuron dan fungsi aktivasi softmax.
23. Compile model: Model dikompilasi dengan menggunakan categorical_crossentropy sebagai fungsi loss, optimizer adam, dan accuracy sebagai metrik evaluasi.
24. Melakukan training model: Model dilatih dengan X dan y_new sebagai input dan target, epoch 22, batch size 64, shuffle True, dan validation split 0.2.
25. Cek akurasi model: Akurasi model dievaluasi dengan menggunakan data validasi atau pengujian. Akurasi ini memberikan indikasi sejauh mana model dapat mengklasifikasikan emosi dengan benar.
26. Menyimpan model: Setelah model selesai dilatih, model disimpan menggunakan fungsi save() dengan nama file 'model_emotion.h5'.

Setelah model disimpan, model dapat diimplementasikan.

#### 5. Validasi dan Evaluasi
Setelah pelatihan model, tahap ini melibatkan validasi dan evaluasi kinerja model.  Validasi dilakukan dengan membagi dataset menjadi subset pelatihan dan subset pengujian, sehingga kinerja model dapat diukur pada data yang belum pernah dilihat sebelumnya.

#### 6. Implementasi dan Uji Coba
Tahap ini melibatkan implementasi model deteksi emosi pada web server menggunakan Flask. Model yang telah dilatih digunakan untuk mendeteksi emosi secara real-time melalui kamera web. Uji coba dilakukan untuk memastikan kinerja model dalam lingkungan yang sesungguhnya dan memperbaiki masalah yang mungkin muncul selama implementasi.

##### Flowchart Implementasi Model pada Web :
```mermaid
flowchart TD
    A[Impor library dan inisialisasi Flask] --> B[Gunakan Cascade Classifier]
    B --> C[Muat model deteksi emosi]
    C --> D[Tentukan label emosi]
    D --> E[Buat fungsi deteksi emosi]
    E --> F[Buat fungsi untuk menghasilkan frame-frame]
    F --> G[Tentukan route '/']
    G --> H[Tentukan route '/video_feed']
    H --> I[Jalankan aplikasi Flask]

```

##### Penjelasan Cara Kerja : 
1. Impor library yang diperlukan dan inisialisasi objek Flask.
2. Menggunakan Cascade Classifier untuk mendeteksi wajah dalam frame.
3. Memuat model deteksi emosi yang telah dilatih.
4. Menentukan label emosi yang akan ditampilkan.
5. Membuat fungsi untuk mendeteksi emosi pada wajah.
6. Membuat fungsi untuk menghasilkan frame-frame yang telah diproses.
7. Menentukan route '/' untuk menampilkan halaman utama.
8. Meneentukan route '/video_feed' sebagai sumber video untuk ditampilkan pada halaman utama.
9. Menjalankan aplikasi Flask.

Dengan menjalankan kode tersebut, Anda dapat melihat tampilan video real-time yang menampilkan  deteksi emosi pada wajah yang terdeteksi melalui kamera web.

#### 7. Analisis Hasil dan Kesimpulan
Tahap akhir R&D melibatkan analisis hasil yang diperoleh dari implementasi model deteksi emosi. Hasil tersebut dievaluasi dan dianalisis dengan membandingkan dengan asumsi dan hipotesis yang diajukan sebelumnya. Kesimpulan ditarik berdasarkan analisis hasil, kinerja model, keberhasilan dalam mencapai tujuan penelitian, dan saran untuk pengembangan lebih lanjut.

# No 10 
##### Link Google Docs Paper : https://docs.google.com/document/d/1eeNQwVsGjitMAvjgZ5RFa5z8X6z_oBg1/edit?usp=sharing&ouid=107036395810341537604&rtpof=true&sd=true

