# No 1
![](https://gitlab.com/jessyfk06/praktikum-sistem-multimedia/-/raw/main/emotion.gif)

##### Link Source Code : https://github.com/JessyFaujiyyahK/UTS-Image

##### Link Youtube : https://youtu.be/8oshUcWf4bQ

# No 2
Aplikasi ini merupakan aplikasi berbasis web untuk mendeteksi emosi berdasarkan gambar, cara kerja aplikasi ini yaitu saat user mengupload suatu gambar berupa wajah seseorang yang memperlihatkan emosi nya ketika si submit maka akan mendeteksi emosi dari gambar tersebut. Selain menggunakan image processing , ukuran gambar yang dideteksi juga akan mengecil dibandingkan dengan file asli.

##### Flowchart :

```mermaid
flowchart TD
    A[Tampilan Upload Gambar] -->|Upload Gambar| B((Aplikasi Flask))
    B -->|Load Library| C[Library Flask, OpenCV, Keras, PIL]
    C -->|Inisialisasi| D((Flask))
    D -->|Set 2 Routes| E{Route /}
    D -->|Set 2 Routes| F{Route /after}
    E -->|Kembalikan tampilan index.html| A
    F -->|Menerima data gambar| G[Menerima data gambar dari server]
    G -->|Simpan request file| H[Simpan request file ke static/file.jpg]
    H -->|Kompresi gambar| I[Membuka file gambar, Kompresi dengan kualitas 50%, Simpan ke static/compressed.jpg]
    I -->|Deteksi Wajah| J[Gambar diklasifikasikan untuk mendeteksi wajah, Jika terdapat wajah, diberi kotak hijau]
    J -->|Potong Gambar| K[Potong wajah yang terdeteksi, Simpan ke static/cropped.jpg]
    K -->|Ubah Ukuran| L[Membaca gambar yang telah dipotong, Ubah ukuran menjadi 48x48 pixel]
    L -->|Bagi Intensitas Warna| M[Bagi intensitas warna gambar dengan 255]
    M -->|Masukkan ke dalam model| N[Gambar diinput ke dalam model]
    N -->|Prediksi Emosi| O[Model memprediksi emosi]
    O -->|Ambil nilai maksimum| P[Ambil nilai maksimum dari prediksi model]
    P -->|Ambil label dari variabel label_map| Q[Ambil label sesuai dengan nilai maksimum dari variabel label_map]
    Q -->|Tampilkan hasil| R[Tampilan hasil prediksi emosi di tampilan after.html]
    R -->|Kembalikan tampilan after.html| S((Selesai))
    D -->|Jalankan Aplikasi Flask| S[app.run dengan mode debug]
```

##### Penjelasan Cara Kerja :
1. Pada tampilan HTML yang menampilkan sebuah tampilan untuk upload gambar yang akan dideteksi emosinya. 
2. Ketika file gambar telah diupload dan tombol `submit` ditekan  
3. File gambar akan dikirim ke aplikasi Flask untuk diolah. 
4. Program memuat beberapa library seperti Flask, OpenCV, keras, dan PIL.
5. Kemudian, program menginisialisasi aplikasi Flask 
6. Setelah itu, program menentukan 2 route yaitu "/" dan "/after".
7. Route "/" akan mengembalikan tampilan template HTML dengan nama "index.html".
8. Route "/after" akan menerima data dari server dan mengirimkan data ke server. Data yang diterima adalah gambar yang telah diupload oleh pengguna.
9. Selanjutnya, program menyimpan request file dan menyimpannya ke dalam file "static/file.jpg".
10. Program kemudian membuka file gambar, mengompresi gambar tersebut dengan kualitas 50%, dan menyimpannya ke dalam file "static/compressed.jpg".
11. Gambar yang telah dikompresi akan dimasukkan ke dalam classifier untuk mendeteksi wajah pada gambar. Jika terdapat wajah, maka akan diberi kotak hijau sebagai penanda wajah yang terdeteksi.
12. Program kemudian memotong wajah yang terdeteksi dari gambar asli dan menyimpannya ke dalam file "static/cropped.jpg".
13. Selanjutnya, program membaca gambar yang telah dipotong dan mengubah dimensinya menjadi 48x48 pixel.
14. Program kemudian membagi intensitas warna gambar dengan 255 agar memudahkan proses ke dalam model.
15. Gambar yang telah diubah dimensinya dan dibagi intensitas warnanya akan dimasukkan ke dalam model.
16. Model yang telah di-load akan memprediksi emosi dari gambar yang telah diinput.
17. Label untuk masing-masing kelas emosi disimpan dalam variabel label_map. 
18. Program akan mengambil nilai maksimum dari prediksi model dan mengambil label dari variabel label_map sesuai dengan nilai maksimum tersebut.
19. Akhirnya, program akan menampilkan hasil prediksi emosi orang yang ada pada gambar di tampilan template HTML dengan nama "after.html".
20. Jalankan aplikasi Flask menggunakan app.run() dengan mode debug.

##### Flowchart Pemodelan Emotion Detection :

```mermaid
flowchart TD
    A[Import library numpy, pandas, dan cv2] --> B[Baca file csv dan tampilkan data pertama]
    B --Periksa panjang data kolom 'pixels'--> C
    C[Import label_map nama-nama emosi] --> D[Tampilkan gambar pertama]
    B --> E[Buat fungsi getData]
    E --> F[Panggil getData]
    F --Ubah nilai X--> G[Normalisasi nilai X]
    G --> H[Ubah nilai label menjadi one-hot encoding]
    H --> I[Tampilkan 5 gambar acak]
    G --> J[Ubah bentuk variabel X]
    J --Import library to_categorical--> K
    H --> K
    K --Buat model neural network--> L[Layer CNN, Layer CNN, Layer CNN, Flatten, Dense]
    L --> M[Compile model]
    M --> N[Lakukan training model]
    N --> O[Simpan model]

```

##### Penjelasan Cara kerja Pemodelan Emotion Detection :

1. Import library numpy, pandas, dan cv2.
2. Menggunakan pandas untuk membaca file csv yang berisi data tentang ekspresi wajah dan menyimpannya ke dalam variabel df. Kemudian menampilkan 5 data pertama dari df menggunakan method head().
3. Melakukan pengecekan panjang data pada kolom 'pixels' pada baris pertama df dengan menggunakan method len(), dan menampilkan hasilnya. Hasilnya adalah 2304, yang merupakan hasil dari 48 x 48.
4. Membuat label_map yang berisi nama-nama emosi, yaitu 'Anger', 'Neutral', 'Fear', 'Happy', 'Sad', dan 'Surprise'.
5. Import library matplotlib.pyplot, dan menampilkan gambar dari baris pertama df dengan menggunakan method imshow(), dan menambahkan label yang sesuai dengan label_map.
6. Membuat fungsi getData(path) yang menerima parameter path berisi path menuju file csv yang berisi data tentang ekspresi wajah. Kemudian membaca file csv tersebut menggunakan pandas dan menyimpannya ke dalam variabel df. Variabel X dan y diinisialisasi dengan list kosong. Variabel-variabel tersebut akan diisi dengan nilai-nilai pixel dan label. Kemudian dilakukan iterasi pada df, dimana setiap iterasi akan mengecek nilai pada kolom 'emotion' pada baris yang sedang diperiksa. Jika nilainya tidak sama dengan 1, maka akan dilakukan pengecekan apakah nilai tersebut sudah mencapai batas maksimum 4000 untuk setiap jenis emosi. Jika belum mencapai, maka nilai pada kolom 'emotion' akan disimpan ke dalam variabel y, dan nilai pada kolom 'pixels' akan diubah menjadi list of integer dan disimpan ke dalam variabel X. Selain itu, nilai untuk jenis emosi tersebut akan diincrement untuk menghitung batas maksimum yang sudah dicapai. Jika nilai pada kolom 'emotion' sama dengan 1, maka tidak akan dilakukan apa-apa.
7. Memanggil fungsi getData() dengan parameter path yang berisi path menuju file csv yang berisi data tentang ekspresi wajah. Hasil yang didapatkan adalah nilai-nilai piksel yang sudah dinormalisasi dan labelnya yang sudah diubah, dan disimpan ke dalam variabel X dan y.
8. Mengubah nilai pada variabel X menjadi float dan membaginya dengan 255.0 untuk melakukan normalisasi.
9. Membuat variabel y_o yang akan diisi dengan nilai label yang sudah diubah dengan mengubah nilai 6 menjadi 1, dan nilai-nilai lainnya tetap sama. Kemudian menampilkan jumlah setiap label menggunakan method unique().
10. Menampilkan 5 gambar secara acak dari variabel X beserta labelnya menggunakan library matplotlib.
11. Mengubah bentuk variabel X menjadi array 4 dimensi dengan ukuran (no_of_images, height, width, coloar_map).
12. Mengimport library to_categorical dari keras.utils.
13. Mengubah nilai pada variabel y_o menjadi bentuk one-hot encoding dengan menggunakan method to_categorical() dari library keras.utils.
14.  Menggunakan Keras untuk membuat model neural network untuk klasifikasi gambar. 
15. Mengambil gambar grayscale dengan ukuran 48 x 48 piksel sebagai input dan mengeluarkan label kelas gambar yang terdiri dari 6 kelas yang berbeda.
16. Import library yang diperlukan: Code diawali dengan mengimport library yang dibutuhkan yaitu Keras dan optimizer untuk mengoptimalkan model.
17. Membuat objek model: Objek model dibuat dengan menggunakan Sequential() yang merupakan struktur model paling sederhana dan sequential, dimana layer-layer neural network ditambahkan secara berurutan.
18. Menambahkan layer Convolutional Neural Network (CNN): Pertama, ditambahkan layer Conv2D dengan 64 kernel, filter berukuran 5x5, fungsi aktivasi 'relu', padding 'same', dan input shape sebesar 48x48x1. Kemudian, ditambahkan lagi Conv2D layer dengan spesifikasi yang sama. Selanjutnya, ditambahkan BatchNormalization dan Activation dengan fungsi aktivasi 'relu'. Terakhir, ditambahkan MaxPooling2D layer dengan pool size 2x2.
19. Menambahkan layer CNN: Ditambahkan lagi layer CNN dengan spesifikasi sama seperti sebelumnya, namun dengan 128 kernel.
20. Menambahkan layer CNN: Ditambahkan lagi layer CNN dengan filter berukuran 3x3, 256 kernel, fungsi aktivasi 'relu', padding 'same', dan dilakukan MaxPooling2D.
21. Melakukan Flatten(): Dilakukan flatten pada output layer sebelumnya agar dapat dihubungkan ke Dense layer.
22. Menambahkan Dense layer: Ditambahkan Dense layer dengan 6 unit neuron dan fungsi aktivasi softmax.
23. Compile model: Model dikompilasi dengan menggunakan categorical_crossentropy sebagai fungsi loss, optimizer adam, dan accuracy sebagai metrik evaluasi.
24.  Melakukan training model: Model dilatih dengan X dan y_new sebagai input dan target, epoch 22, batch size 64, shuffle True, dan validation split 0.2.
25. Menyimpan model: Setelah model selesai dilatih, model disimpan menggunakan fungsi save() dengan nama file 'model.h5'.

# No 3
Emotion detection atau deteksi emosi adalah salah satu aplikasi dari kecerdasan buatan yang bertujuan untuk mengenali emosi pada wajah atau suara manusia. Aspek kecerdasan buatan yang terkait dengan emotion detection antara lain:

1. Computer vision: Aspek ini berkaitan dengan pengenalan wajah dan ekspresi wajah yang berbeda. Teknologi deep learning, khususnya Convolutional Neural Networks (CNN) telah terbukti efektif dalam mengenali ekspresi wajah.

2. Machine learning: Aspek ini mencakup pengembangan model machine learning yang dapat mempelajari pola dari data yang diberikan dan menghasilkan hasil yang akurat dalam mengenali emosi. Teknik seperti Neural Networks dapat digunakan untuk mengklasifikasikan emosi berdasarkan data yang diberikan.


# No 4
![](https://gitlab.com/jessyfk06/praktikum-sistem-multimedia/-/raw/main/sound_effect.gif)

##### Link Source Code : https://github.com/JessyFaujiyyahK/UTS-Audio

##### Link Youtube : https://youtu.be/zc0dGJ3vq98

# No 5
Aplikasi ini merupakan aplikasi berbasis web untuk menambahkan sound effect pada audio yang user miliki. Cara kerja aplikasi ini yaitu saat user mengupload audio yang ingin diberi sound effect, kemudian pilih sound effect apa yang akan ditambahkan pada audio user. Setelah menekan button upload, maka sound effect akan diproses yang menghasilkan gabungan antara audio user dan sound effect. Ukuran dari hasil audio sound effect tersebut juga telah di compress dengan bitrate 96k. 

##### Flowchart : 

```mermaid
flowchart TD
    A[Tampilan Upload Audio dan pemilihan sound effect] -->|Upload Audio| B((Aplikasi Flask))
    B -->|Load Library| C[Library Flask, render_template, request, jsonify, send_from_directory, os, secure_filename, dan AudioSegment]
    C -->|Inisialisasi| D((Flask))
    D -->|Set 2 Routes| E{Route /}
    D -->|Set 2 Routes| F{Route /upload}
    E -->|Kembalikan tampilan upload.html| A
    F --> G[Menentukan jalur untuk mengunggah file]
    G --> H[Simpan dalam konfigurasi aplikasi]
    H --> |Membuat rute| I[upload.html dengan method POST]
    I --> |Membuat rute| J[Hasil efek suara ke upload.html dengan method POST]
    J --> K{Memeriksa file dalam request}
    K --> |Tidak| L[Respons JSON dengan pesan 'error'] 
    K --> |Ya|M[Menyimpan audio file upload dengan secure_filename bernama before.mp3]
    M --> N{Memeriksa apakah ada nama file yang sama}
    N --> |Ya| P[Akan dihapus terlebih dahulu sebelum file baru disimpan]
    N --> |Tidak| Q[File akan disimpan]
    P --> R[Menyimpan file before.mp3]
    Q --> R
    R --> S[Memuat file audio yang diunggah menggunakan AudioSegment]
    S --> T[Muat file sound effect audio yang disimpan di folder effect]
    T --> U[Mendapatkan value dari form sound effectyang dipilih] 
    U --> V[Overlays file audio efek suara dengan file audio utama yang diunggah]
    V --> |Mengatur parameter audio| W[set_frame_rate, set_sample_width, and set_channels]
    W --> X[Simpan file audio overlay ke dalam folder statis dengan nama after.mp3 dan melakukan kompres dengan bitrate 96k]
    X --> Y[Menampilkan halaman play.html dengan overlay audio untuk mendengarkan]
    Y --> Z[app.run dalam mode debug]
    D -->|Jalankan Aplikasi Flask| Z
    Z --> a(end)
```

##### Penjelasan Cara Kerja : 
1. Pengguna membuka aplikasi dan memilih menu "Sound Effect".
2. Aplikasi menampilkan halaman "Sound Effect" yang berisi form untuk mengunggah file dan memilih efek suara.
3. Pengguna memilih file suara dan memilih efek suara pada dropdown menu.
4. Pengguna menekan tombol "Upload".
5. Aplikasi memproses file suara yang diunggah dan mengolahnya sesuai dengan efek suara yang dipilih.
6. Proses file audio diproses pada app.py
7. Menggunakan librarry  Flask, render_template, request, jsonify, send_from_directory, os, secure_filename, dan AudioSegment.
8. Buat instance Flask dengan nama app.
9. Menentukan path untuk folder upload file dan simpan di konfigurasi app.
10. Membuat route untuk halaman utama dengan nama index() dan template upload.html.
11. Membuat route untuk upload file dengan nama upload() dan method POST.
12. Mengecek apakah terdapat file dalam request. Jika tidak ada, akan mengirimkan JSON response dengan pesan error.
13. Menyimpan file audio yang diunggah dengan menggunakan secure_filename untuk membuat nama file yang aman.
14. Jika terdapat file dengan nama yang sama di path, file tersebut akan dihapus terlebih dahulu sebelum file baru disimpan.
15. Memuat file audio yang diunggah menggunakan AudioSegment.
16. Memuat file audio efek suara yang tersimpan pada folder effect.
17. Mengambil nilai dari form select yang dipilih.
18. Melakukan overlay file audio efek suara dengan file audio utama yang diunggah menggunakan overlay.
19. Setel parameter audio menggunakan set_frame_rate, set_sample_width, dan set_channels.
20. Simpan file audio yang telah di-overlay ke dalam folder static dengan nama after.mp3.
21. Tampilkan halaman play.html dengan audio hasil overlay untuk didengarkan.
22. Jalankan aplikasi Flask menggunakan app.run() dengan mode debug.

# No 6
Kecerdasan buatan (AI) dapat memiliki beberapa aspek yang dapat digunakan dalam pengembangan produk sound effect. Aspek kecerdasan buatan dalam produk sound effect salah satunya yaitu pengolahan suara, AI dapat digunakan untuk melakukan pengolahan suara seperti pengurangan decibels ini memungkinkan untuk menciptakan keseimbangan suara antara audio user serta sound effect yang akan di tambahkan.
